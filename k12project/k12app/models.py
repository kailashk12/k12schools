# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models



class Section(models.Model):

    section = models.CharField(max_length=15)
    sec_id= models.AutoField(primary_key="true",default='')

class subject(models.Model):
    subject=models.CharField(max_length=20)
    sid= models.AutoField(primary_key="true",default='')

class Grade(models.Model):
    grade = models.CharField(max_length=15)
    Section= models.ForeignKey('Section',on_delete=models.CASCADE,default='')
    subject = models.ForeignKey('subject',on_delete=models.CASCADE, default='')
    def __str__(self):
        return self.grade
    
class  student(models.Model):
    name=models.CharField(max_length=20)
    grade=models.ForeignKey('Grade',on_delete=models.CASCADE,default='')
    Section=models.CharField(max_length=1)
    email= models.EmailField(_(""), max_length=254)
    phonenumber= models.PhoneNumberField(_("") , max_length=10)
    image=models.ImageField(_(""), upload_to=None, height_field=None, width_field=None, max_length=None)

       
           
       
           class Meta:
               abstract = True
       